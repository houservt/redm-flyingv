# RedM Flying V Bird Resource

## Description
Add resrouce to your server and the 'ensure redm-flyingv-master' line to your server.cfg to allow folks to fly around like a bird.

## Requirements
None really, but take a look at line 76 if you're not using redemrp_skin -- you'll want to adjust that to direct to an event/command that loads the persons previous skin/clothing if desired.

## Installation
- Clone/download the resource folder into your server's resources folder
- Add ```ensure redm-flyingv-master``` in server.cfg

## How to use
Use command ```/flylikea [pedname]``` to tranform into bird. (list of ped models -- https://www.mod-rdr.com/wiki/ped-search/)

Walk around as normal, shift to sprint, etc.
Use [spacebar] to enter flight-mode.
In flight-mode your bird will fly straight & level when no keys are pressed.


    Use [W] to fly higher
    Use [S] to fly lower
    Use [A] to turn left
    Use [D] to turn right
    Use [Spacebar] to exit flight-mode and re-enter walking mode.

Landing takes some practice; basically you use [S] until you're right above the ground, then hit [Spacebar]. If you hit [Spacebar] mid-flight, you'll "land" in mid-air, but will fall back to the ground (no fall dmg).\
Note: Simultanious keypresses are not registered; i.e. you cannot press W + D to fly higher *while* turning right.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/SjBfHJpAaX8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

[![Teaser Video](https://i.ibb.co/6sMhjbF/flyingv.png)](https://www.youtube.com/watch?v=YEcvkrGb5XI "FlyingV")